name := "Lift 2.6 starter template"

version := "0.0.4"

organization := "net.liftweb"

scalaVersion := "2.11.7"

resolvers ++= Seq("snapshots"     at "https://oss.sonatype.org/content/repositories/snapshots",
                "releases"        at "https://oss.sonatype.org/content/repositories/releases"
                )

seq(webSettings :_*)

unmanagedResourceDirectories in Test <+= (baseDirectory) { _ / "src/main/webapp" }

scalacOptions ++= Seq("-deprecation", "-unchecked")

libraryDependencies ++= {
  val liftVersion = "2.6.2"
  Seq(
    "net.liftweb"       %% "lift-webkit"         % liftVersion        % "compile",
    "net.liftweb"       %% "lift-json"           % liftVersion        % "compile",
    "net.liftweb"       %% "lift-testkit"        % liftVersion        % "test->default",
    "net.liftmodules"   %% "lift-jquery-module_2.6" % "2.8",
    "org.eclipse.jetty" % "jetty-webapp"         % "8.1.17.v20150415"  % "container,test",
    "org.eclipse.jetty" % "jetty-plus"           % "8.1.17.v20150415"  % "container,test", // For Jetty Config
    "org.eclipse.jetty.orbit" % "javax.servlet"  % "3.0.0.v201112011016" % "container,test" artifacts Artifact("javax.servlet", "jar", "jar"),
    "ch.qos.logback"    % "logback-classic"      % "1.1.3",
    "org.specs2" % "specs2_2.11" % "2.3.11" % "test",

    "org.squeryl"       % "squeryl_2.11"         % "0.9.5-7",
    "mysql"             % "mysql-connector-java" % "5.1.6",
    "net.databinder.dispatch" %% "dispatch-core" % "0.11.2"
  )
}

scalacOptions in Test ++= Seq("-Yrangepos")
parallelExecution in Test := false
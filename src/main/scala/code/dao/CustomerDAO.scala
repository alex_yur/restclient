package code.dao

import code.dao.AddressDAO._
import code.db.MySchema._
import code.model.Customer
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException
import net.liftweb.common._
import org.squeryl.PrimitiveTypeMode
import org.squeryl.PrimitiveTypeMode._

import scala.util.Try

/**
  * Customer DAO layer.
  */
object CustomerDAO extends Loggable {

  /**
    * Adds customer to DB.
    *
    * @param customer customer entity to add
    * @return
    */
  def add(customer: Customer): Box[Customer] = {
    inTransaction {
      try {
        val innerCustomer = customers.insert(customer)
        Full(innerCustomer)
      } catch {
        case r: RuntimeException =>
          r.getCause match {
            case exception: MySQLIntegrityConstraintViolationException =>
              exception.getErrorCode match {
                case DAOErrors.Duplicate => Failure(s"Duplicate customers not allowed", Full(r), null) ~> DAOErrors.Duplicate
                case _ => Failure(s"Duplicate customers not allowed", Full(r), null) ~> DAOErrors.Internal
              }
            case _ =>
              Failure(s"Duplicate customers not allowed", Full(r), null) ~> DAOErrors.Internal
          }
        case ex: Throwable => Failure("Insert in DB Error", Full(ex), null) ~> DAOErrors.Internal
      }
    }
  }

  /**
    * Return all customers from DB.
    *
    * @return all customers from DB
    */
  def getAll: Box[List[Customer]] = {
    inTransaction {
      from(customers)(s => select(s)) match {
        case x if x.nonEmpty => Full(x.toList)
        case e if e.isEmpty => Full(e.toList)
        case ex: Throwable => Failure("Exception caught.", Full(ex), null)
      }
    }
  }

  /**
    * Return customer by id.
    *
    * @param id if to get customer
    * @return box with customer
    */
  def getById(id: Long): Box[Customer] = {
    inTransaction {
      from(customers)(s => where(s.id === id) select s).toList match {
        case f if f.nonEmpty => Full(f.head)
        case e if e.isEmpty => Empty
        case _ => Failure("Error while querying occurred", null, null)
      }
    }
  }

  /**
    * Deletes customer by id.
    *
    * @param id id to delete customer
    * @return 0 - no customer, 1 - customer deleted
    */
  def delete(id: Long): Boolean = {
    inTransaction {
      customers.deleteWhere(x => x.id === id) match {
        case 0 => false
        case 1 => true
      }
    }
  }

  /**
    * Update customer.
    *
    * @param customer to update
    * @return 0 - no customer, 1 - customer updated
    */
  def update(id: Long, customer: Customer): Boolean = {
    inTransaction {
      PrimitiveTypeMode.update(customers)(c =>
        where(c.id === id)
          set(c.name := customer.name,
          c.surname := customer.surname,
          c.age := customer.age)) match {
        case 0 => false
        case 1 => true
      }
    }
  }
}

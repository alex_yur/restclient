package code.dao

import java.sql.SQLException

import code.db.MySchema._
import code.model.Address
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException
import net.liftweb.common._
import org.squeryl.PrimitiveTypeMode
import org.squeryl.PrimitiveTypeMode._


/**
  * Address DAO.
  */
object AddressDAO extends Loggable {

  implicit def int2bool(int: Int): Boolean = {
    int match {
      case 0 => false
      case 1 => true
      case _ => throw new IllegalArgumentException("")
    }
  }

  /**
    * Adds addres to DB
    * @param address address to add
    * @return long id of added address
    */
  def add(address: Address): Box[Address] = {
    inTransaction {
      try {
        val innerAddress = addresses.insert(address)
        Full(innerAddress)
      } catch {
        case r: RuntimeException =>
          r.getCause match {
            case exception: MySQLIntegrityConstraintViolationException =>
              exception.getErrorCode match {
                case DAOErrors.Duplicate => Failure(s"Duplicate addresses not allowed", Full(r), null) ~> DAOErrors.Duplicate
                case DAOErrors.WrongCustomerId => Failure(s"No customer with ${address.customerId}", Full(r), null) ~> DAOErrors.WrongCustomerId
                case _ => Failure("Insert in DB Error", Full(exception), null) ~> DAOErrors.Internal
              }
            case _ => Failure("Insert in DB Error", Full(r), null) ~> DAOErrors.Internal
          }
        case ex: Throwable => Failure("Insert in DB Error", Full(ex), null) ~> DAOErrors.Internal
      }
    }
  }

  /**
    * Return address from DB by id
    *
    * @param id id for address search
    * @return box with address
    */
  def getById(id: Long): Box[Address] = {
    inTransaction {
      from(addresses)(s => where(s.id === id) select s) match {
        case f if f.nonEmpty => Full(f.head)
        case e if e.isEmpty => Empty
        case ex: Throwable => Failure("Error while querying occurred", Full(ex), null)
      }
    }
  }


  /**
    * Return Address list by Customer ID
    *
    * @param id customer Id
    * @return Address list
    */
  def getByCustomerId(id: Long): Box[List[Address]] = {
    inTransaction {
      from(addresses)(s => where(s.customerId === id) select s) match {
        case f if f.nonEmpty => Full(f.toList)
        case e if e.isEmpty => Full(e.toList)
        case ex: Throwable => Failure("Error while querying occurred", Full(ex), null)
      }
    }
  }

  /**
    * Return list af all addresses
    *
    * @return box with address list
    */
  def getAll: Box[List[Address]] = {
    inTransaction {
      from(addresses)(s => select(s)) match {
        case x if x.nonEmpty => Full(x.toList)
        case e if e.isEmpty => Empty
        case ex: Throwable => Failure("Exception caught", Full(ex), null)
      }
    }
  }

  /**
    * Update address.
    *
    * @param id id of address to update
    * @param address address data
    * @return true if address was updated
    */
  def update(id: Long, address: Address): Boolean = {
    inTransaction {
      PrimitiveTypeMode.update(addresses)(c =>
        where(c.id === id)
          set(c.city := address.city,
          c.street := address.street,
          c.number := address.number,
          c.customerId := address.customerId))
    }
  }

  /**
    * Delete address.
    *
    * @param id id of address to delete
    * @return true if address was deleted
    */
  def delete(id: Long): Boolean = {
    inTransaction {
      addresses.deleteWhere(x => x.id === id)
    }
  }
}

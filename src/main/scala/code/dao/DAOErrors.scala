package code.dao

/**
  * DAO Errors ENUM.
  */
object DAOErrors extends Enumeration{
  /**
    * Duplicate SLQ error.
    */
  val Duplicate = 1062

  /**
    * Foreign key constraint SQL error.
    */
  val WrongCustomerId = 1452

  val Internal = Value

}

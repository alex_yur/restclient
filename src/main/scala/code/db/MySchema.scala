package code.db

import code.model.{Address, Customer}
import org.squeryl.PrimitiveTypeMode._
import org.squeryl._

/**
  * DB Schema.
  */
object MySchema extends Schema {
  /**
    * Customer table.
    */
  val customers = table[Customer]

  /**
    * Address table
    */
  val addresses = table[Address]

  /**
    * Defines  foreign key.
    */
  val customerToAddresses =
    oneToManyRelation(customers, addresses).
      via((c, a) => c.id === a.customerId)

  override def applyDefaultForeignKeyPolicy(foreignKeyDeclaration: ForeignKeyDeclaration) =
    foreignKeyDeclaration.constrainReference

  /**
    * Assign that entities with foreign key will be deleted.
    */
  customerToAddresses.foreignKeyDeclaration.constrainReference(onDelete cascade)

  /**
    * Sets customer id field autoincrement
    */
  on(customers)(customers => declare(
    customers.id is autoIncremented
  ))

  /**
    * Sets address id field autoincrement
    */
  on(addresses)(address => declare(
    address.id is autoIncremented,
    columns(address.city, address.street, address.number) are(indexed, unique)
  ))
}

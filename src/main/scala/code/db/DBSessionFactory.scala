package code.db

import code.model.{Address, Customer}
import net.liftweb.common.Loggable
import net.liftweb.util.Props
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.adapters.MySQLInnoDBAdapter
import org.squeryl.{Session, SessionFactory}

/**
  * Data base session factory.
  */
object DBSessionFactory extends Loggable {
  val databaseUsername = Props.get("username").openOrThrowException("No db User Name assigned in properties")
  val databasePassword = Props.get("password").openOrThrowException("No db password assigned in properties")
  val databaseConnection = Props.get("db").openOrThrowException("No data base assigned in properties")

  /**
    * Creates db session.
    */
  def startDatabaseSession(): Unit = {
    logger.debug("Creating DB session")
    Class.forName("com.mysql.jdbc.Driver")
    SessionFactory.concreteFactory = Some(() => Session.create(
      java.sql.DriverManager.getConnection(databaseConnection, databaseUsername, databasePassword),
      new MySQLInnoDBAdapter)
    )

    logger.debug("DB session created")
  }

  /**
    * Creates DB.
    */
  def prepareDB(): Unit = {
    transaction {
      try {
        MySchema.drop
        logger.debug("Creating DB Schema")
        MySchema.create
        logger.debug("DB Schema has been created")
      } catch {
        case s: Exception =>
          logger.debug("DB is already created")
      }
    }
  }

  /**
    * Fill DB with dummy objects.
    */
  def fillDB(): Unit = inTransaction {
    MySchema.customers.insert(Customer(0, "Sasha", "Paha", 23))
    MySchema.customers.insert(Customer(0, "Vanya", "Some", 5))
    MySchema.customers.insert(Customer(0, "Foo", "Bar", 66))
    MySchema.customers.insert(Customer(0, "Medved", "Preved", 21))

    MySchema.addresses.insert(Address(0, "Kiev", "Artema", 58, Some(1)))
    MySchema.addresses.insert(Address(0, "Kiev", "Vorovskigo", 11, Some(1)))
    MySchema.addresses.insert(Address(0, "Lvov", "Pl.Runok", 2, Some(3)))
    MySchema.addresses.insert(Address(0, "Odessa", "Derebasovskaya", 13, Some(2)))
    MySchema.addresses.insert(Address(0, "Dnepropetrovsk", "Titova", 32, Some(2)))
  }

  def fillTestDB(): Unit = inTransaction{
    MySchema.customers.insert(Customer(0, "Sasha", "Paha", 23))
  }

}

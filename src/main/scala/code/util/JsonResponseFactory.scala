package code.util

import code.model.SerializableModel
import net.liftweb.http.{JsonResponse, LiftResponse}
import net.liftweb.json.JsonDSL._

/**
  * Json Response factory.
  */
object JsonResponseFactory {

  /**
    * HTTP response codes.
    */
  val StatusOk = 200
  val StatusBadRequest = 400
  val StatusCreated = 201
  val StatusNOContent = 204
  val StatusInternalServerError = 500
  val StatusConflict = 409
  val StatusNotFound = 404
  val StatusRequestTimeout = 408

  /**
    * Response messages.
    */
  val NoDataFoundMessage = "No data found"
  val ErrorIllegalArgsMessage = "Error" -> "Illegal arguments"
  val InternalServerErrorMessage = "Error" -> "Internal server error"
  val RequestTimeout = "Error" -> "Request timeout"
  val Error = "Error"
  val Conflict = "Conflict"
  val Success = "Success"

  def getStatusNotFound: LiftResponse = {
    JsonResponse(NoDataFoundMessage, StatusNotFound)
  }

  def getStatusNotFound(msg: String): LiftResponse = {
    JsonResponse(msg, StatusNotFound)
  }

  def getStatusInternalError(msg: String): LiftResponse = {
    JsonResponse(msg, StatusInternalServerError)
  }

  def getStatusCreated(obj: SerializableModel): LiftResponse = {
    JsonResponse(obj.toJSON, StatusCreated)
  }

  def getStatusBadRequest: LiftResponse = {
    JsonResponse(ErrorIllegalArgsMessage, StatusBadRequest)
  }

  def getStatusBadRequest(msg: String): LiftResponse = {
    JsonResponse(Error -> msg, StatusBadRequest)
  }

  def getStatusInternalServerError: LiftResponse = {
    JsonResponse(InternalServerErrorMessage, StatusInternalServerError)
  }

  def getStatusSuccess: LiftResponse = {
    JsonResponse(Success, StatusOk)
  }

  def getStatusRequestTimeout: LiftResponse ={
    JsonResponse(RequestTimeout, StatusRequestTimeout)
  }

  def getStatusConflict(msg: String): LiftResponse ={
    JsonResponse(Conflict -> msg, StatusConflict)
  }
}

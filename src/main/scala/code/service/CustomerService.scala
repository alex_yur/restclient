package code.service

import code.dao.CustomerDAO
import code.model.Customer
import net.liftweb.common.Box

/**
  * Created by aleksey-yur on 25.12.15.
  */
object CustomerService {
  /**
    * Address DAO.
    */
  val dao = CustomerDAO

  /**
    * Add address.
    *
    * @param customer address to add
    * @return address
    */
  def add(customer: Customer): Box[Customer] = {
    dao.add(customer)
  }

  /**
    * Delete address.
    *
    * @param id id of address to delete
    * @return true if delete succeeded
    */
  def delete(id: Long): Boolean = {
    dao.delete(id)
  }

  /**
    * Return all addresses.
    *
    * @return all addresses
    */
  def getAll: Box[List[Customer]] = {
    dao.getAll
  }

  /**
    * Return address.
    *
    * @param id id of address
    * @return address
    */
  def getById(id: Long): Box[Customer] = {
    dao.getById(id)
  }

  /**
    * Update address
    *
    * @param id id of address to update
    * @param customer data of update
    * @return true if update succeeded
    */
  def update(id: Long, customer: Customer): Boolean = {
    dao.update(id, customer)
  }
}

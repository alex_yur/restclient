package code.service

import code.dao.AddressDAO
import code.model.Address
import net.liftweb.common.Box

/**
  * Address service.
  */
object AddressService {

  /**
    * Address DAO.
    */
  val dao = AddressDAO

  /**
    * Add address.
    *
    * @param address address to add
    * @return address
    */
  def add(address: Address): Box[Address] = {
    dao.add(address)
  }

  /**
    * Delete address.
    *
    * @param id id of address to delete
    * @return true if delete succeeded
    */
  def delete(id: Long): Boolean = {
    dao.delete(id)
  }

  /**
    * Return all addresses.
    *
    * @return all addresses
    */
  def getAll: Box[List[Address]] = {
    dao.getAll
  }

  /**
    * Return address.
    *
    * @param id id of address
    * @return address
    */
  def getById(id: Long): Box[Address] = {
    dao.getById(id)
  }


  /**
    * Return list of addresses.
    *
    * @param id id of customer
    * @return list of addresses
    */
  def getByCustomerId(id: Long): Box[List[Address]] = {
    dao.getByCustomerId(id)
  }

  /**
    * Update address
    *
    * @param id id of address to update
    * @param address data of update
    * @return true if update succeeded
    */
  def update(id: Long, address: Address): Boolean = {
    dao.update(id, address)
  }
}

package code.controller

import code.handler.{AddressRequestHandler, CustomerDetailsRequestHandler, CustomerRequestHandler}
import code.model.Customer
import code.util.JsonResponseFactory
import net.liftweb.common.Loggable
import net.liftweb.http.rest.RestHelper
import net.liftweb.json.JsonAST.{JObject, JInt, JField, JString}
import net.liftweb.json.JsonDSL._
import net.liftweb.util.Helpers.AsInt

import scala.language.postfixOps


/**
  * Rest controller
  */
object RestController extends RestHelper with Loggable {

  /**
    * Customer endpoints.
    */
  serve("api" / "customer" prefix {

    /** id :: PUT :: json  */
    case AsInt(id) :: _ JsonPut json -> request =>
      logger.info(s"Requested customer updater with id $id, json $json")
      CustomerRequestHandler.update(id, json.extract[Customer])

    case AsInt(id) :: "address" :: _ JsonGet _ =>
      logger.info(s"Requested address list of customer with id $id")
      CustomerRequestHandler.getAddresses(id)

    /** id :: GET  */
    case AsInt(id) :: _ JsonGet _ =>
      logger.info(s"GET Requested contacts with id: $id")
      CustomerRequestHandler.getById(id)

    /** GET  */
    case _ JsonGet _ =>
      logger.info("GET Requested contacts list.")
      CustomerRequestHandler.getAll

    /** id :: POST (add address for certain customer */
    case AsInt(id) :: _ JsonPost json -> request =>
      json match {
        case JObject(l) =>  AddressRequestHandler.add(JObject(l :+ JField("customerId", JInt(id))))
        case _ => JsonResponseFactory.getStatusBadRequest
      }

    /** POST :: json */
    case Nil JsonPost json -> request =>
      CustomerRequestHandler.create(json)

    /** id :: DELETE  */
    case AsInt(id) :: _ JsonDelete _ =>
      logger.debug("DELETE on customer with id : " + id)
      CustomerRequestHandler.delete(id)

    case req =>
      req.json_? match {
        case true => JsonResponseFactory.getStatusBadRequest(s"Request type not supported or illegal arguments")
        case false =>
          logger.warn(s"Requested format ${req.contentType} not supported")
          JsonResponseFactory.getStatusBadRequest(s"Format ${req.contentType} not supported")
      }
  })


  /**
    * Address endpoints.
    */
  serve("api" / "address" prefix {

    /** id :: GET */
    case AsInt(id) :: _ JsonGet request =>
      logger.info(s"GET address request by id : $id")
      AddressRequestHandler.getById(id)

    /** GET */
    case _ JsonGet _ =>
      logger.info("GET all addresses request")
      AddressRequestHandler.getAll

    /** POST :: json */
    case _ JsonPost json -> request =>
      logger.info(s"Add new address request json: $json")
      AddressRequestHandler.add(json)

    /** id :: DELETE */
    case AsInt(id) :: _ JsonDelete _ =>
      logger.info(s"Delete address id: $id")
      AddressRequestHandler.delete(id)

    /** id :: PUT :: json */
    case AsInt(id) :: _ JsonPut json -> request =>
      logger.info(s"Update address id: $id , json: $json")
      AddressRequestHandler.update(id, json)

    case req =>
      req.json_? match {
        case true => JsonResponseFactory.getStatusBadRequest(s"Request type not supported or illegal arguments")
        case false =>
          logger.warn(s"Requested format ${req.contentType} not supported")
          JsonResponseFactory.getStatusBadRequest(s"Format ${req.contentType} not supported")
      }
  })

  /**
    * Customer details endpoints.
    */
  serve("api" / "customer-details" prefix {

    case _ JsonGet request =>
      logger.info("Customer details GET request")
      CustomerDetailsRequestHandler.getAll

    case _ JsonPost json -> request =>
      logger.info(s"Customer details POST request json: $json")
      CustomerDetailsRequestHandler.add(json)

    case req =>
      req.json_? match {
        case true => JsonResponseFactory.getStatusBadRequest(s"Request type not supported or illegal arguments")
        case false =>
          logger.warn(s"Requested format ${req.contentType} not supported")
          JsonResponseFactory.getStatusBadRequest(s"Format ${req.contentType} not supported")
      }
  })

}

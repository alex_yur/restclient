package code.handler

import code.dao.DAOErrors
import code.model.{AddressSerializer, Address}
import code.service.AddressService
import code.util.JsonResponseFactory
import code.util.JsonResponseFactory._
import net.liftweb.common._
import net.liftweb.http.{JsonResponse, LiftResponse}
import net.liftweb.json.JsonAST.JValue
import net.liftweb.json.JsonDSL._
import net.liftweb.json.{DefaultFormats, MappingException}

/**
  * Address request handler.
  *
  * Handle requests from address api path.
  */
object AddressRequestHandler extends Loggable {

  /**
    * Return address
    * @param id of address
    * @return address
    */
  def getById(id: Long): LiftResponse = {
    AddressService.getById(id) match {
      case Full(address) =>
        logger.trace(s"Address $address returned")
        JsonResponse(address.toJSON)
      case Empty =>
        logger.debug(s"No address with id $id found")
        getStatusNotFound
      case Failure(msg, f, c) =>
        logger.error(f)
        getStatusInternalServerError
    }
  }

  /**
    * Return list of Addresses
    * @return list of Addresses
    */
  def getAll: LiftResponse = {
    AddressService.getAll match {
      case Full(l) =>
        JsonResponse(for (cur <- l) yield cur.toJSON)
      case Empty => getStatusNotFound
      case Failure(msg, ex, l) =>
        logger.error(ex)
        getStatusInternalServerError
    }
  }

  /**
    * Add address
    * @param json json value of address
    * @return
    */
  def add(json: JValue): LiftResponse = {
    try {
      implicit val formats = DefaultFormats + new AddressSerializer
      logger.trace(s"Json received $json")

      val address = json.extract[Address]

      if(address.customerId.isEmpty){
        throw new MappingException("No field 'customerId'")
      }

      logger.trace(s"Extracted address $address")

      AddressService.add(address) match {
        case Full(f) =>
          logger.info(s"$address added to DB")
          getStatusCreated(f)
        case Empty => getStatusInternalServerError
        case ParamFailure(msg, e, f, param) =>
          param match {
          case DAOErrors.Duplicate => getStatusConflict(s"Address $address already exists")
          case DAOErrors.WrongCustomerId => getStatusConflict(s"No customer with id = ${address.customerId.get}")
          case DAOErrors.Internal =>
            logger.error(e)
            getStatusInternalServerError
        }
      }
    } catch {
      case e: MappingException =>
        logger.warn(e)
        getStatusBadRequest(e.getMessage)
      case o: Exception =>
        logger.error(o)
        getStatusInternalServerError
    }
  }

  /**
    * Delete request handler.
    *
    * @param id to delete
    * @return Lift responce
    */
  def delete(id: Long): LiftResponse = {
    AddressService.delete(id) match {
      case true => getStatusSuccess
      case false => getStatusNotFound
    }
  }

  /**
    * Update request handler.
    *
    * @param id if to update
    * @param json with update data
    * @return Lift responce
    */
  def update(id: Long, json: JValue): LiftResponse = {
    implicit val formats = DefaultFormats
    val address = json.extract[Address]

    AddressService.update(id, address) match {
      case true => getStatusSuccess
      case false => getStatusNotFound
    }
  }
}

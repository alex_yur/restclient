package code.handler

import code.dao.{AddressDAO, CustomerDAO}
import code.model.{AddressSerializer, Address, Customer, CustomerDetails}
import code.service.{AddressService, CustomerService}
import code.util.JsonResponseFactory._
import net.liftweb.common.{Empty, Failure, Full, Loggable}
import net.liftweb.http.{JsonResponse, LiftResponse}
import net.liftweb.json._

/**
  * Customer Details request handler.
  */
object CustomerDetailsRequestHandler extends Loggable {

  /**
    * Return all customers with nested details.
    *
    * @return all customers with nested details.
    */
  def getAll: LiftResponse = {
    CustomerDAO.getAll map (_.map (c =>
        CustomerDetails(Some(c.id), c.name, c.surname,
          c.age, AddressDAO.getByCustomerId(c.id)))) match {
      case Full(l) => JsonResponse(JArray(for (x <- l) yield x.toJSON))
      case Empty =>
        logger.warn("No results")
        getStatusNotFound
      case Failure(msg, ex, l) =>
        logger.error(ex)
        getStatusInternalServerError
    }
  }

  /**
    * Add customer and nested addresses
    *
    * @param json json from request
    * @return Lift response
    */
  def add(json: JValue): LiftResponse = {
    implicit val formats = DefaultFormats + new AddressSerializer
    try {
      val customer = json.extract[Customer]

      println(s"Customer : $customer")

      CustomerService.add(customer).map(x =>
        for (a <- (json \\ "addresses").children.map(ad =>
          ad.extract[Address])) yield AddressService.add(a.copy(customerId = Some(x.id)))) match {
        case Full(s) =>
          logger.debug(s"$customer added")
          getStatusCreated(json.extract[CustomerDetails])
        case Empty =>
          logger.debug(s"Failed to add $customer")
          getStatusBadRequest
        case Failure(msg, ex, l) =>
          logger.error(ex)
          getStatusInternalServerError
      }
    } catch {
      case me: MappingException =>
        logger.warn(me)
        getStatusBadRequest("Incorrect arguments")
      case e: Exception =>
        logger.error(e)
        getStatusInternalServerError
    }
  }
}

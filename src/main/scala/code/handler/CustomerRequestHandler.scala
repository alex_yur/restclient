package code.handler

import code.dao.DAOErrors
import code.handler.AddressRequestHandler._
import code.model.Customer
import code.service.{AddressService, CustomerService}
import code.util.JsonResponseFactory._
import net.liftweb.common._
import net.liftweb.http.{JsonResponse, LiftResponse}
import net.liftweb.json.JsonAST.JValue
import net.liftweb.json.JsonDSL._
import net.liftweb.json._

/**
  * Customer request handler.
  */
object CustomerRequestHandler extends Loggable {
  implicit val formats = DefaultFormats
  /**
    * Adds new client.
    *
    * @param json json received from request
    * @return lift response
    */
  def create(json: JValue): LiftResponse = {
    var customer: Customer = null
    try {
      customer = json.extract[Customer]

      CustomerService.add(customer) match {
        case Full(f) =>
          logger.debug(s"Customer $customer created")
          getStatusCreated(f)
        case Empty => getStatusInternalServerError
        case ParamFailure(msg, e, f, param) =>
          param match {
            case DAOErrors.Duplicate => getStatusConflict(s"Address $customer already exists")
            case DAOErrors.Internal =>
              logger.error(e)
              getStatusInternalServerError
          }
      }
    }catch {
      case e: MappingException =>
        logger.error(e)
        getStatusBadRequest("Illegal arguments")
      case th: Throwable =>
        logger.error(th)
        getStatusInternalServerError
    }
  }

  /**
    * Get customer by id.
    *
    * @param id id to search with
    * @return lift response with json customer or error
    */
  def getById(id: Long): LiftResponse = {
    CustomerService.getById(id) match {
      case Full(s) =>
        JsonResponse(s.toJSON, StatusOk)
      case Empty =>
        logger.warn(s"No user with id: $id")
        getStatusNotFound(s"No user with requested id: $id")
      case Failure(msg, ex, l) =>
        getStatusInternalError(msg)
    }
  }

  /**
    * Return all available customers.
    *
    * @return all available customers
    */
  def getAll: LiftResponse = {
    CustomerService.getAll match {
      case Full(l) => l match {
        case empty if l.isEmpty =>
          logger.trace("No customers found")
          getStatusNotFound
        case full if l.nonEmpty =>
          logger.trace(s"${l.length} - customers found")
          JsonResponse(for (cur <- l) yield cur.toJSON)
      }
      case Empty =>
        logger.error("Unexpected empty box")
        getStatusInternalServerError
      case Failure(msg, ex, l) =>
        logger.error(ex)
        getStatusInternalServerError
    }
  }

  /**
    * Delete customer by id.
    *
    * @param id if to delete customer
    * @return Lift response with success or no content status
    */
  def delete(id: Int): LiftResponse = {
    CustomerService.delete(id) match {
      case true => getStatusSuccess
      case false => getStatusNotFound(s"No customer with requested id: $id")
    }
  }

  /**
    * Update customer.
    *
    * @param id id of customer
    * @param customer customer to update
    * @return Lift response with result
    */
  def update(id: Long, customer: Customer): LiftResponse = {
    CustomerService.update(id, customer) match {
      case true => getStatusSuccess
      case false => getStatusNotFound(s"No customer with requested id: $id")
    }
  }

  def getAddresses(id: Long): LiftResponse = {
    AddressService.getByCustomerId(id) match {
      case Full(list) =>
        list.isEmpty match {
          case true => getStatusNotFound
          case false => JsonResponse(for (cur <- list) yield cur.toJSON)
        }
      case Failure(msg, ex, l) =>
        logger.error(ex)
        getStatusInternalServerError
      case Empty =>
        logger.error("Unexpected empty box")
        getStatusInternalServerError
    }
  }
}

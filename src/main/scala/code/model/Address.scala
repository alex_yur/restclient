package code.model


import net.liftweb.json.{JsonAST, DefaultFormats, CustomSerializer}
import net.liftweb.json.JsonAST._
import net.liftweb.json.JsonDSL._

/**
  * Address model.
  */

case class Address(override val id: Long = 0L, city: String, street: String,
                   number: Int, customerId: Option[Long] = Some(0)) extends BaseModel with SerializableModel {

  def this(city: String, street: String, number: Int, customerId: Option[Long]) = this(0L, city, street, number, customerId)


  override def toString: String = {
    city + " " + street + " " + customerId.get
  }
}

class AddressSerializer extends CustomSerializer[Address](formats => ( {
  case JObject(ad) =>
    implicit val formats = DefaultFormats

    if (!(ad \\ "id").isInstanceOf[JInt]) {
      JObject(ad :+ JField("id", JInt(0))).extract[Address]
    } else {
      JObject(ad).extract[Address]
    }
}, {
  case a: Address => a.toJSON
})
)



package code.model

import net.liftweb.json.{DefaultFormats, JsonParser, Serialization}

/**
  * Serialize entities
  */
trait SerializableModel {
  /**
    * Entity to JSON converter.
    *
    * @return JSON representation of entity
    */
  def toJSON = {
    implicit val formats = DefaultFormats
    JsonParser.parse(Serialization.write(this))
  }
}

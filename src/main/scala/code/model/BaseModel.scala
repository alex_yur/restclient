package code.model

import org.squeryl.KeyedEntity

/**
  * Squeryl base model.
  */
class BaseModel extends KeyedEntity[Long] {

  /**
    * Entity ID
    */
  val id: Long = 0
}


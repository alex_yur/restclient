package code.model

/**
  * Customer model.
  */
case class Customer(override val id: Long = 0L, name: String, surname: String, age: Int) extends BaseModel with SerializableModel {

  def this(name: String, surname: String, age: Int) = this(0, name, surname, age)

  override def toString: String = {
    id + " " + name + " " + surname + " " + age
  }
}



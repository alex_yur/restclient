package code.model

/**
  * Customer details model.
  */
case class CustomerDetails(id: Option[Long], name: String, surname: String,
                           age: Int, addresses: Option[List[Address]]) extends SerializableModel

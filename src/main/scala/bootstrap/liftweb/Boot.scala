package bootstrap.liftweb

import code.controller.RestController
import code.db.DBSessionFactory
import code.util.JsonResponseFactory
import net.liftweb.common.Full
import net.liftweb.http._
import net.liftweb.http.rest.RestContinuation
import net.liftweb.util.Helpers._
import net.liftweb.util.Schedule

/**
  * A class that's instantiated early and run.  It allows the application
  * to modify lift's environment
  */
class Boot {

  /**
    * Request timeout, set to 10 seconds
    */
  val timeout = 4000

  def boot {
    // where to search snippet
    LiftRules.addToPackages("code")

    LiftRules.statelessDispatch.append {
      case r => RestContinuation.async {
        f => {
          Schedule.schedule(() => f(JsonResponseFactory.getStatusRequestTimeout), timeout)
          f(RestController(r)().openOr(JsonResponseFactory.getStatusInternalServerError))
        }
      }
    }

    DBSessionFactory.startDatabaseSession()

    DBSessionFactory.prepareDB()

    DBSessionFactory.fillDB()
  }
}
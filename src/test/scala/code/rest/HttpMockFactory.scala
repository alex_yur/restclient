package code.rest

import net.liftweb.http.ContentType
import net.liftweb.json.JsonAST.JValue
import net.liftweb.mocks.MockHttpServletRequest

/**
  * Http Mock factory
  */
object HttpMockFactory {

  val testUrl = "http://localhost:8080"

  def getHttpMock(url: String, method: String): MockHttpServletRequest = {
    val req = new MockHttpServletRequest(testUrl)
    req.path = url
    req.method = method
    req
  }

  def getHttpMock(url: String, method: String, jValue: JValue): MockHttpServletRequest = {
    val req = new MockHttpServletRequest(testUrl)
    req.path = url
    req.method = method
    req.body_= (jValue)
    req
  }

  def getHttpMock(url: String, method: String, jValue: JValue, contentType: String): MockHttpServletRequest = {
    val req = new MockHttpServletRequest(testUrl)
    req.path = url
    req.method = method
    req.body_= (jValue)
    req.contentType = contentType
    req
  }
}

package code.rest

import code.controller.RestController
import code.db.{MySchema, DBSessionFactory}
import code.model.Customer
import code.util.JsonResponseFactory
import net.liftweb.common.Full
import net.liftweb.http.rest.RestContinuation
import net.liftweb.http.{JsonResponse, LiftRules, OkResponse}
import net.liftweb.json.JsonAST.{JValue, JObject}
import net.liftweb.json.JsonDSL._
import net.liftweb.mocks.MockHttpServletRequest
import net.liftweb.mockweb.WebSpec
import HttpMockFactory._



object WebSpecSpecBoot {
  def boot() {
    println("WebSpecSpec Booting up")

    LiftRules.addToPackages("code")

    println("WebSpecSpec Boot complete")
  }
}

class IRestServiceTest extends WebSpec(WebSpecSpecBoot.boot) {

  sequential

  val testUrl = "http://localhost:8080"
  val mockReq = new MockHttpServletRequest(testUrl)

  DBSessionFactory.startDatabaseSession()
  DBSessionFactory.prepareDB()
  DBSessionFactory.fillTestDB()

  val user = ("name" -> "TestNmae") ~ ("surname" -> "TestSurname") ~ ("age" -> 2)
  val fakeUser = ("name" -> "TestNmae") ~ ("surname" -> "TestSurname")
  val expectedUser = ("id" -> 2) ~ ("name" -> "TestNmae") ~ ("surname" -> "TestSurname") ~ ("age" -> 2)

  val address = ("city" -> "Kiev") ~ ("street" -> "Artema") ~ ("number" -> 12) ~ ("customerId" -> 1)
  val addressWithoutStreet = ("city" -> "Kiev") ~ ("number" -> 12) ~ ("customerId" -> 1)

  val addressWithFalseCustomerId = ("city" -> "Kiev") ~ ("street" -> "Artema") ~ ("number" -> 12) ~ ("customerId" -> 10)

  "Customer API" should {
    "add user" withReqFor getHttpMock("/api/customer", "POST", user) in {
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 201
        case _ => false
      }
    }
    "return bad request code" withReqFor getHttpMock("/api/customer", "POST", fakeUser) in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 400
        case _ => false
      }
    }
    "get user with id 2" withReqFor getHttpMock("/api/customer/2", "GET") in {
      req => RestController(req)() match {
        case Full(d) => d ==== JsonResponse(expectedUser, Nil, Nil, 200)
        case _ => false
      }
    }
    "delete user with id 2" withReqFor getHttpMock("/api/customer/2", "DELETE") in {
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 200
        case _ => false
      }
    }
    "return 404 error when trying to delete user with id 2 again" withReqFor getHttpMock("/api/customer/2", "DELETE") in {
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 404
        case _ => false
      }
    }
    "return 400 error when content type is invalid" withReqFor getHttpMock("/api/customer", "POST", user, "application/xml") in {
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 400
        case _ => false
      }
    }
    "return 400 if operation is not supported" withReqFor getHttpMock("/api/customer", "OPTION") in {
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 400
        case _ => false
      }
    }
  }

  "Address API" should {
    "add address" withReqFor getHttpMock("/api/address", "POST", address) in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 201
        case _ => false
      }
    }
    "return conflict when trying to add address with false customerId" withReqFor getHttpMock("/api/address", "POST", addressWithFalseCustomerId) in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 409
        case _ => false
      }
    }
    "return conflict when trying to add duplicate address" withReqFor getHttpMock("/api/address", "POST", address) in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 409
        case _ => false
      }
    }
    "delete address with id 1" withReqFor getHttpMock("/api/address/1", "DELETE") in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 200
        case _ => false
      }
    }
    "return 404 if no address with id 1" withReqFor getHttpMock("/api/address/1", "GET") in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 404
        case _ => false
      }
    }
    "return bad request when field street is missing" withReqFor getHttpMock("/api/address", "POST", addressWithoutStreet) in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 400
        case _ => false
      }
    }
    "add address" withReqFor getHttpMock("/api/address", "POST", address) in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 201
        case _ => false
      }
    }
    "should delete address on delete related user" withReqFor getHttpMock("/api/customer/1", "DELETE") in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 200
        case _ => false
      }
    }
    "check if address was deleted" withReqFor getHttpMock("/api/address/1", "GET") in{
      req => RestController(req)() match {
        case Full(d) => d.toResponse.code == 404
        case _ => false
      }
    }
  }
}
